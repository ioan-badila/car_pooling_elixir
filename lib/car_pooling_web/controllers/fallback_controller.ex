defmodule CarPoolingWeb.FallbackController do
  @moduledoc """
  Translates controller action results into valid `Plug.Conn` responses.

  See `Phoenix.Controller.action_fallback/1` for more details.
  """
  use CarPoolingWeb, :controller

  def call(conn, {:error, :not_found}) do
    conn
    |> resp(404, "")
  end

  def call(conn, {:error, :is_invalid, message}) do
    conn
    |> put_status(400)
    |> put_view(CarPoolingWeb.ErrorView)
    |> render(:"400", message: message)
  end
end
