defmodule CarPoolingWeb.ServiceController do
  @moduledoc false

  use CarPoolingWeb, :controller
  import CarPoolingWeb.Controllers.Validation

  action_fallback CarPoolingWeb.FallbackController

  def load_cars(conn, %{"_json" => cars}) do
    with {:ok, cars} <- cast_cars(cars),
         :ok <- CarPooling.load_cars(cars) do
      send_resp(conn, 200, "OK")
    end
  end

  def journey(conn, params) do
    with {:ok, {group_id, people}} <- cast_group(params),
         :ok <- CarPooling.journey(group_id, people) do
      send_resp(conn, 200, "")
    end
  end

  def dropoff(conn, params) do
    with {:ok, group_id} <- cast_group_id(params),
         :ok <- CarPooling.dropoff(group_id) do
      send_resp(conn, 200, "")
    end
  end

  def locate(conn, params) do
    with {:ok, group_id} <- cast_group_id(params) do
      case CarPooling.locate(group_id) do
        {:ok, :waiting} -> send_resp(conn, :no_content, "")
        {:ok, %{} = car} -> json(conn, car)
        error -> error
      end
    end
  end
end
