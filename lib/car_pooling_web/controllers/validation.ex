defmodule CarPoolingWeb.Controllers.Validation do
  @moduledoc false

  @typep group_id :: non_neg_integer()
  @typep people :: 1..6

  @doc """
  Casts input with a list of cars
  """
  @spec cast_cars([map]) :: {:ok, map} | {:error, :is_invalid, String.t()}
  def cast_cars(input) when is_list(input) do
    result =
      Enum.reduce_while(
        input,
        %{},
        fn entry, cars ->
          with {:ok, id, seats} <- cast_car(entry),
               :ok <- ensure_unique_id(cars, id) do
            {:cont, Map.put(cars, id, seats)}
          else
            error ->
              {:halt, error}
          end
        end
      )

    case result do
      {:error, _, _} = err -> err
      cars -> {:ok, cars}
    end
  end

  def cast_cars(_input), do: {:error, :is_invalid, "Not a list of cars"}

  defp cast_car(%{"id" => id, "seats" => seats})
       when is_integer(id) and id > 0 and seats in [4, 5, 6] do
    {:ok, id, seats}
  end

  defp cast_car(car), do: {:error, :is_invalid, "Invalid car: #{inspect(car)}"}

  defp ensure_unique_id(cars, id) do
    if Map.has_key?(cars, id),
      do: {:error, :is_invalid, "Duplicate car: #{id}"},
      else: :ok
  end

  @doc """
  Casts a group of people
  """
  @spec cast_group(map) :: {:ok, {group_id, people}} | {:error, :is_invalid, String.t()}
  def cast_group(%{"id" => id, "people" => people})
      when is_integer(id) and is_integer(people) and id > 0 and people > 0 and people < 7 do
    {:ok, {id, people}}
  end

  def cast_group(_group) do
    {:error, :is_invalid, "Invalid group"}
  end

  @doc """
  Casts a group id
  """
  @spec cast_group_id(term) :: {:ok, group_id} | {:error, :is_invalid, String.t()}
  def cast_group_id(%{"ID" => id}), do: go_cast_group_id(id)
  def cast_group_id(params), do: {:error, :is_invalid, "Invalid group_id: #{inspect(params)}"}

  defp go_cast_group_id(id) do
    case Integer.parse(id) do
      {parsed, ""} when parsed > 0 -> {:ok, parsed}
      _ -> {:error, :is_invalid, "Invalid group_id: #{inspect(id)}"}
    end
  end
end
