defmodule CarPoolingWeb.Plugs.StatusPlug do
  @moduledoc false

  import Plug.Conn

  def init(opts), do: opts

  def call(%Plug.Conn{request_path: "/status"} = conn, _opts) do
    if conn.method == "GET" do
      conn
      |> put_resp_content_type("text/plain")
      |> send_resp(200, ~S({"status": "ok"}))
      |> halt
    else
      conn
    end
  end

  def call(conn, _opts) do
    conn
  end
end
