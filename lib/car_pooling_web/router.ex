defmodule CarPoolingWeb.Router do
  use CarPoolingWeb, :router

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", CarPoolingWeb do
    pipe_through :api
    put "/cars", ServiceController, :load_cars
    post "/journey", ServiceController, :journey
    post "/dropoff", ServiceController, :dropoff
    post "/locate", ServiceController, :locate

    # Method not allowed
    [&post/3, &put/3, &patch/3, &delete/3, &connect/3, &trace/3]
    |> Enum.each(fn verb -> verb.("/*path", InvalidMethodController, :match) end)
  end
end
