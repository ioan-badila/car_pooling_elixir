defmodule CarPooling do
  @moduledoc """
  CarPooling keeps the contexts that define your domain
  and business logic.
  """

  alias CarPooling.Service.{GroupsOfPeople, Cars}
  alias CarPooling.Service.PoolingService

  @type group_id :: non_neg_integer()
  @type num_people :: 1..6
  @type car :: %{id: non_neg_integer(), seats: 4 | 5 | 6}

  @doc """
  Load the list of available cars in the service and remove all previous data
  (existing journeys and cars).
  """
  defdelegate load_cars(cars), to: PoolingService

  @doc """
  Register and schedule a new journey for a group of people
  """
  @spec journey(group_id, num_people) :: :ok | {:error, :is_invalid, String.t()}
  def journey(group_id, num_people) do
    if GroupsOfPeople.add_new(group_id),
      do: PoolingService.journey(group_id, num_people),
      else: {:error, :is_invalid, "The group is already scheduled or on journey"}
  end

  @doc """
  Return the car the group is traveling with, or no car if they are still waiting to be served.
  """
  @spec locate(group_id()) :: {:ok, car} | {:ok, :waiting} | {:error, :not_found}
  def locate(group_id) do
    case GroupsOfPeople.get_status(group_id) do
      nil -> {:error, :not_found}
      :waiting -> {:ok, :waiting}
      {:on_journey, car_id, _occupied_seats} -> Cars.fetch(car_id)
    end
  end

  @doc """
  Group of people request to be dropped off. Whether they traveled or not.
  """
  @spec dropoff(group_id()) :: :ok | {:error, :not_found}
  defdelegate dropoff(group_id), to: PoolingService
end
