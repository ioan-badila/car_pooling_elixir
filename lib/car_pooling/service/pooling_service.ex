defmodule CarPooling.Service.PoolingService do
  @moduledoc """
  This is the context responsible with car pooling management
  """

  use GenServer

  alias CarPooling.Service.Cars
  alias CarPooling.Service.GroupsOfPeople

  @type group_id :: non_neg_integer()
  @type num_people :: 1..6
  @type car :: %{id: non_neg_integer(), seats: 4 | 5 | 6}

  def start_link(_opts) do
    GenServer.start_link(__MODULE__, nil, name: __MODULE__)
  end

  @doc """
  Load the list of available cars in the service and remove all previous data
  (existing journeys and cars).
  """
  def load_cars(cars) do
    GenServer.call(__MODULE__, {:load_cars, cars})
  end

  @doc """
  Register and schedule a new journey for a group of people
  """
  @spec journey(group_id, num_people) :: :ok
  def journey(group_id, num_people) do
    GenServer.call(__MODULE__, {:journey, group_id, num_people})
  end

  @doc """
  Group of people request to be dropped off. Whether they traveled or not.
  """
  @spec dropoff(group_id()) :: :ok | {:error, :not_found}
  def dropoff(group_id) do
    GenServer.call(__MODULE__, {:dropoff, group_id})
  end

  @doc """
  Return the car the group is traveling with, or no car if they are still waiting to be served.
  """
  @spec locate(group_id()) :: {:ok, car} | {:ok, :waiting} | {:error, :not_found}
  def locate(group_id) do
    case GroupsOfPeople.get_status(group_id) do
      nil -> {:error, :not_found}
      :waiting -> {:ok, :waiting}
      {:on_journey, car_id, _occupied_seats} -> Cars.fetch(car_id)
    end
  end

  #
  # Server implementation
  #

  def init(_opts), do: {:ok, nil}

  def handle_call({:load_cars, cars}, _, _state) do
    GroupsOfPeople.reset()
    Cars.reset()
    Enum.each(cars, &Cars.add_new/1)

    reply_ok()
  end

  def handle_call({:journey, group_id, num_people}, _, _state) do
    case Cars.find_car_for(num_people) do
      nil -> GroupsOfPeople.queue_group(group_id, num_people)
      car_id -> GroupsOfPeople.set_status(group_id, {:on_journey, car_id, num_people})
    end

    reply_ok()
  end

  def handle_call({:dropoff, group_id}, _, _state) do
    case GroupsOfPeople.get_status(group_id) do
      nil ->
        reply({:error, :not_found})

      {:on_journey, car_id, num_people} ->
        GroupsOfPeople.remove(group_id)
        available_seats = Cars.restore_seats(car_id, num_people)
        resolve_waiting(car_id, available_seats)
        reply_ok()

      _ ->
        # Nothing to do with waiting people
        reply_ok()
    end
  end

  defp resolve_waiting(car_id, available_seats) do
    case GroupsOfPeople.find_and_delete_for(available_seats) do
      nil ->
        Cars.update(car_id, available_seats)

      {group_id, num_people} ->
        GroupsOfPeople.set_status(group_id, {:on_journey, car_id, num_people})

        resolve_waiting(car_id, available_seats - num_people)
    end
  end

  defp reply_ok(), do: {:reply, :ok, nil}
  defp reply(x), do: {:reply, x, nil}
end
