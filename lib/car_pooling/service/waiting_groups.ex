defmodule CarPooling.Service.WaitingGroups do
  @moduledoc """
  This holds the table with the waiting groups
  """

  def child_spec(_arg) do
    %{
      id: __MODULE__,
      start: {Eternal, :start_link, [__MODULE__, [:ordered_set, {:read_concurrency, true}]]}
    }
  end

  @doc """
  Queue group
  """
  @spec queue_group(group_id :: non_neg_integer(), num_people :: non_neg_integer()) :: :ok
  def queue_group(group_id, num_people) do
    :ets.update_counter(:counters, :groups_count, {num_people + 1, 1})
    priority = System.monotonic_time()
    :ets.insert(__MODULE__, {priority, group_id, num_people})
    :ok
  end

  @doc """
  Resets the waiting people queue
  """
  @spec reset() :: :ok
  def reset() do
    :ets.delete_all_objects(__MODULE__)
    :ets.insert(:counters, {:groups_count, 0, 0, 0, 0, 0, 0})
    :ok
  end

  @doc """
  Tries to find a car for the specified number of people.
  If it is found it will be removed from the queue.
  """
  @spec find_and_delete_for(num_people :: non_neg_integer()) ::
          {group_id :: non_neg_integer(), num_of_people :: non_neg_integer()}
          | nil
  def find_and_delete_for(num_people) do
    if suitable_group(num_people),
      do: find_group_for(num_people),
      else: nil
  end

  # credo:disable-for-lines:13
  defp suitable_group(available_seats) do
    [groups_count] = :ets.lookup(:counters, :groups_count)

    case {available_seats, groups_count} do
      {n, {_, x, _, _, _, _, _}} when x != 0 and n in 1..6//1 -> true
      {n, {_, _, x, _, _, _, _}} when x != 0 and n in 2..6//1 -> true
      {n, {_, _, _, x, _, _, _}} when x != 0 and n in 3..6//1 -> true
      {n, {_, _, _, _, x, _, _}} when x != 0 and n in 4..6//1 -> true
      {n, {_, _, _, _, _, x, _}} when x != 0 and n in [5, 6] -> true
      {6, {_, _, _, _, _, _, x}} when x != 0 -> true
      _ -> false
    end
  end

  defp find_group_for(num_people) do
    result =
      :ets.select(
        __MODULE__,
        [
          {{:_, :_, :"$1"}, [{:"=<", :"$1", num_people}], [:"$_"]}
        ],
        1
      )

    case result do
      {[group], _} -> delete_group(group)
      _ -> nil
    end
  end

  defp delete_group({id, group_id, num_people}) do
    :ets.delete(__MODULE__, id)
    :ets.update_counter(:counters, :groups_count, {num_people + 1, -1})
    {group_id, num_people}
  end
end
