defmodule CarPooling.Service.Counters do
  @moduledoc false

  def child_spec(_arg) do
    %{
      id: __MODULE__,
      start: {Eternal, :start_link, [:counters, [:set, {:read_concurrency, true}]]}
    }
  end

  def update_counters(previous) do
    :ets.update_counter(:counters, :seats_count, {previous + 2, -1})
  end

  def update_counters(previous, new) do
    :ets.update_counter(:counters, :seats_count, [{new + 2, 1}, {previous + 2, -1}])
  end
end
