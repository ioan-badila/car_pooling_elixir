defmodule CarPooling.Service.Supervisor do
  @moduledoc false

  use Supervisor

  def start_link(init_arg) do
    Supervisor.start_link(__MODULE__, init_arg, name: __MODULE__)
  end

  @impl true
  def init(_init_arg) do
    children = [
      CarPooling.Service.Cars,
      CarPooling.Service.Counters,
      {CarPooling.Service.CarsBucket, [0]},
      {CarPooling.Service.CarsBucket, [1]},
      {CarPooling.Service.CarsBucket, [2]},
      {CarPooling.Service.CarsBucket, [3]},
      {CarPooling.Service.CarsBucket, [4]},
      {CarPooling.Service.CarsBucket, [5]},
      {CarPooling.Service.CarsBucket, [6]},
      CarPooling.Service.GroupsOfPeople,
      CarPooling.Service.WaitingGroups,
      CarPooling.Service.PoolingService
    ]

    Supervisor.init(children, strategy: :one_for_all)
  end
end
