defmodule CarPooling.Service.Cars do
  @moduledoc """
  This is the context responsible with cars management
  """

  alias CarPooling.Service.{Counters, CarsBucket}

  @typep car_id :: non_neg_integer()
  @typep seats :: 4..6
  @type car :: %{id: car_id, seats: seats}

  def child_spec(_arg) do
    %{
      id: __MODULE__,
      start: {Eternal, :start_link, [:cars, [:set, {:read_concurrency, true}]]}
    }
  end

  @doc """
  Returns the specified car
  """
  @spec fetch(car_id) :: {:ok, car} | {:error, :not_found}
  def fetch(car_id) do
    case :ets.lookup(:cars, car_id) do
      [{id, seats, _}] -> {:ok, %{id: id, seats: seats}}
      _ -> {:error, :not_found}
    end
  end

  @doc """
  Adds a new car and puts it in the correct bucket
  """
  @spec add_new({car_id, seats}) :: boolean()
  def add_new({car_id, seats}) do
    :ets.insert_new(:cars, {car_id, seats, seats})
    CarsBucket.insert(seats, car_id)
  end

  @doc """
  Updates seats availability and puts the car in the new bucket
  """
  @spec update(car_id, seats) :: boolean()
  def update(car_id, available_seats) do
    :ets.update_element(:cars, car_id, {3, available_seats})
    CarsBucket.insert(available_seats, car_id)
  end

  @doc """
  Resets the cars table and the buckets
  """
  @spec reset() :: :ok
  def reset() do
    :ets.delete_all_objects(:cars)
    CarsBucket.reset()
    :ok
  end

  @doc """
  Returns an available car_id if found.
  Also removes the car from previous bucket and puts it in the new one
  Updates the car availability.
  """
  @spec find_car_for(non_neg_integer()) :: car_id | nil
  def find_car_for(num_people) do
    case suitable_car_bucket(num_people) do
      nil ->
        nil

      bucket ->
        remaining_seats = bucket - num_people
        car_id = pop_first_car(bucket)
        update_car_availability(car_id, remaining_seats)
        Counters.update_counters(bucket, remaining_seats)
        car_id
    end
  end

  # credo:disable-for-lines:13
  defp suitable_car_bucket(num_people) do
    [bucket_counts] = :ets.lookup(:counters, :seats_count)

    case {num_people, bucket_counts} do
      {1, {_, _, x, _, _, _, _, _}} when x != 0 -> 1
      {n, {_, _, _, x, _, _, _, _}} when x != 0 and n in [1, 2] -> 2
      {n, {_, _, _, _, x, _, _, _}} when x != 0 and n in 1..3 -> 3
      {n, {_, _, _, _, _, x, _, _}} when x != 0 and n in 1..4 -> 4
      {n, {_, _, _, _, _, _, x, _}} when x != 0 and n in 1..5 -> 5
      {n, {_, _, _, _, _, _, _, x}} when x != 0 and n in 1..6 -> 6
      _ -> nil
    end
  end

  defp pop_first_car(bucket) do
    bucket_table = CarsBucket.bucket_table(bucket)
    car_id = :ets.first(bucket_table)
    :ets.delete(bucket_table, car_id)
    car_id
  end

  defp update_car_availability(car_id, seats) do
    bucket_table = CarsBucket.bucket_table(seats)
    :ets.insert(bucket_table, {car_id})

    :ets.update_element(:cars, car_id, {3, seats})
  end

  @doc """
  Restores the seats for the specified car
  Returns the new seat availability for the car
  """
  @spec restore_seats(car_id, seats) :: seats
  def restore_seats(car_id, freed_seats) do
    [{car_id, _seats, prev_seats_availability}] = :ets.lookup(:cars, car_id)
    prev_seats_availability_bucket = CarsBucket.bucket_table(prev_seats_availability)
    :ets.delete(prev_seats_availability_bucket, car_id)
    :ets.update_counter(:counters, :seats_count, {prev_seats_availability + 2, -1})
    prev_seats_availability + freed_seats
  end
end
