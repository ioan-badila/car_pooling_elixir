defmodule CarPooling.Service.GroupsOfPeople do
  @moduledoc """
  This is the context responsible with groups of people management
  """

  alias CarPooling.Service.WaitingGroups

  def child_spec(_arg) do
    %{
      id: __MODULE__,
      start: {Eternal, :start_link, [__MODULE__, [:set, {:read_concurrency, true}]]}
    }
  end

  @doc """
  Resets the groups of people and the waiting people queue
  """
  @spec reset() :: :ok
  def reset() do
    WaitingGroups.reset()
    :ets.delete_all_objects(__MODULE__)
    :ok
  end

  @doc false
  def get_status(group_id) do
    case :ets.lookup(__MODULE__, group_id) do
      [{_id, status}] -> status
      _ -> nil
    end
  end

  @doc false
  def add_new(group_id) do
    :ets.insert_new(__MODULE__, {group_id, :waiting})
  end

  @doc false
  def set_status(group_id, status) do
    :ets.insert(__MODULE__, {group_id, status})
    :ok
  end

  @doc false
  def remove(group_id) do
    :ets.delete(__MODULE__, group_id)
    :ok
  end

  defdelegate queue_group(group_id, num_of_people), to: WaitingGroups
  defdelegate find_and_delete_for(available_seats), to: WaitingGroups
end
