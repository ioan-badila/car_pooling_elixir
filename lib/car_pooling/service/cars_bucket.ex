defmodule CarPooling.Service.CarsBucket do
  @moduledoc """
  This holds the table with the cars buckets
  """

  @typep bucket_id :: 0..6

  def child_spec([bucket_id]) do
    table_name = bucket_table(bucket_id)

    %{
      id: table_name,
      start: {Eternal, :start_link, [table_name, [:set, {:read_concurrency, true}]]}
    }
  end

  @doc """
  Adds a car in the specified bucket and updates the counter
  """
  @spec insert(bucket_id, non_neg_integer()) :: boolean()
  def insert(bucket_id, car_id) do
    :ets.update_counter(:counters, :seats_count, {bucket_id + 2, 1})

    bucket_id
    |> bucket_table()
    |> :ets.insert_new({car_id})
  end

  @doc """
  Resets all buckets and seats_count
  """
  @spec reset() :: :ok
  def reset() do
    Enum.each(0..6, fn id ->
      id |> bucket_table() |> :ets.delete_all_objects()
    end)

    :ets.insert(:counters, {:seats_count, 0, 0, 0, 0, 0, 0, 0})

    :ok
  end

  @doc false
  def bucket_table(id), do: String.to_atom("cars_bucket_#{id}")
end
