defmodule CarPooling.Application do
  @moduledoc false

  use Application

  def start(_type, _args) do
    children = [
      CarPooling.Service.Supervisor,
      CarPoolingWeb.Telemetry,
      {Phoenix.PubSub, name: CarPooling.PubSub},
      CarPoolingWeb.Endpoint
    ]

    opts = [strategy: :one_for_one, name: CarPooling.Supervisor]
    Supervisor.start_link(children, opts)
  end

  def config_change(changed, _new, removed) do
    CarPoolingWeb.Endpoint.config_change(changed, removed)
    :ok
  end
end
