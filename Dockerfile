FROM elixir:1.12.0-alpine

ADD docker/ /
ADD _build/prod/ /rel

RUN set -xe \
  \
  && echo "--- Install runtime deps ---" \
  && apk add --no-cache tini bash \
  \
  && echo "--- Setup OTP release ---" \
  && mkdir /app \
  && tarball=$(find rel/ -type f -name '*.tar.gz' | tail -n 1) \
  && tar -xzf $tarball -C /app \
  \
  && echo "--- Cleanup ---" \    
  && rm -rf /rel

EXPOSE 9091

CMD ["/sbin/tini", "/entrypoint.sh"]