# Car Pooling Service Challenge

Design/implement a system to manage car pooling.

At MyCar we provide the service of taking people from point A to point B.
So far we have done it without sharing cars with multiple groups of people.
This is an opportunity to optimize the use of resources by introducing car
pooling.

You have been assigned to build the car availability service that will be used
to track the available seats in cars.

Cars have a different amount of seats available, they can accommodate groups of
up to 4, 5 or 6 people.

People requests cars in groups of 1 to 6. People in the same group want to ride
on the same car. You can take any group at any car that has enough empty seats
for them. If it's not possible to accommodate them, they're willing to wait until
there's a car available for them. Once a car is available for a group
that is waiting, they should ride.

Once they get a car assigned, they will journey until the drop off, you cannot
ask them to take another car (i.e. you cannot swap them to another car to
make space for another group).

In terms of fairness of trip order: groups should be served as fast as possible,
but the arrival order should be kept when possible.
If group B arrives later than group A, it can only be served before group A
if no car can serve group A.

For example: a group of 6 is waiting for a car and there are 4 empty seats at
a car for 6; if a group of 2 requests a car you may take them in the car.
This may mean that the group of 6 waits a long time,
possibly until they become frustrated and leave.

## API

To simplify the challenge and remove language restrictions, this service must
provide a REST API which will be used to interact with it.

This API must comply with the following contract:

### GET /status

Indicate the service has started up correctly and is ready to accept requests.

Responses:

- **200 OK** When the service is ready to receive requests.

### PUT /cars

Load the list of available cars in the service and remove all previous data
(existing journeys and cars). This method may be called more than once during
the life cycle of the service.

**Body** _required_ The list of cars to load.

**Content Type** `application/json`

Sample:

```json
[
  {
    "id": 1,
    "seats": 4
  },
  {
    "id": 2,
    "seats": 6
  }
]
```

Responses:

- **200 OK** When the list is registered correctly.
- **400 Bad Request** When there is a failure in the request format, expected
  headers, or the payload can't be unmarshalled.

### POST /journey

A group of people requests to perform a journey.

**Body** _required_ The group of people that wants to perform the journey

**Content Type** `application/json`

Sample:

```json
{
  "id": 1,
  "people": 4
}
```

Responses:

- **200 OK** or **202 Accepted** When the group is registered correctly
- **400 Bad Request** When there is a failure in the request format or the
  payload can't be unmarshalled.

### POST /dropoff

A group of people requests to be dropped off. Whether they traveled or not.

**Body** _required_ A form with the group ID, such that `ID=X`

**Content Type** `application/x-www-form-urlencoded`

Responses:

- **200 OK** or **204 No Content** When the group is unregistered correctly.
- **404 Not Found** When the group is not to be found.
- **400 Bad Request** When there is a failure in the request format or the
  payload can't be unmarshalled.

### POST /locate

Given a group ID such that `ID=X`, return the car the group is traveling
with, or no car if they are still waiting to be served.

**Body** _required_ A url encoded form with the group ID such that `ID=X`

**Content Type** `application/x-www-form-urlencoded`

**Accept** `application/json`

Responses:

- **200 OK** With the car as the payload when the group is assigned to a car.
- **204 No Content** When the group is waiting to be assigned to a car.
- **404 Not Found** When the group is not to be found.
- **400 Bad Request** When there is a failure in the request format or the
  payload can't be unmarshalled.

## Requirements

- The service should be as efficient as possible.
  It should be able to work reasonably well with at least $`10^4`$ / $`10^5`$ cars / waiting groups.
  Explain how you did achieve this requirement.
- You are free to modify the repository as much as necessary to include or remove
  dependencies, subject to tooling limitations above.
- Document your decisions using MRs or in this very README adding sections to it,
  the same way you would be generating documentation for any other deliverable.
  We want to see how you operate in a quasi real work environment.

## Assumptions

- The solution should be optimized for performance
- When a group requests a dropoff the group is removed from the system
- Requesting a journey with a group id that is already registered will return an error

## Solution

There are two actions that presents more interest:

- When a group requests for a journey first look for a suitable car and put the group on a ride.
  If none found the group has to be queued in the waiting lists until a suitable car is available.
- When a group requests a dropoff the group is removed from the system and try to find and assign the car to waiting groups suitable.

From this a few things have been noticed:

- In order of keeping track of the cars use a map with car_id and seats_available.
- In order of keeping track of seats availability use a list with cars ordered by seats available.
- In order of keeping track of the groups use a map with group_id and number of people.
- In order to serve the first groups use a list ordered by the arriving time.

All these could be easily kept in a GenServer (or even an Agent).

Although for loading cars, schedule journeys and dropoffs serialization was needed, for locating a group there is no need of it.
To achieve this the list with groups were moved out of the state of GenServer to an ets table.

Another problem is that manipulating large lists are very expensive (time and memory wise) so a better approach is to use ets (with the small penalty of copying the data).
In this regard, the ets tables would look something like this:

- group_of_people: {group_id, :waiting | {:on_journey, car_id, num_people}}, :set
- waiting_groups: {priority :: monotonic_time, group_id, num_people}, :ordered_set
- cars: {car_id, seats}, :set
- available_seats: {{available_seats, car_id}}, :ordered_set

When scheduling a journey is easy to do a select with limit 1 over the available_seats to find the first car with suitable seats efficiently.
However, because select is an O(n) operation id does not scale well at all. And also there is the possibility of scanning the entire table without finding a car. A solution to this is to put cars in buckets and keep track of the each bucket count. By doing this we known upfront if there is a suitable car. And if there is is just a matter of popping out of the bucket and assigning to the group.

(In the code there is a CarBucket0, this is not really needed but just a technicality to make the code look nicer as otherwise this exception should be treated)

When dropping a group and reassigning the car to one or more waiting groups a select with limit 1 over the waiting_groups table could be done. But again, this select is no different then the other one in terms of performance. The problem here is that it is not really possible or efficient to put the waiting groups in bucket. However by keeping track of counts of groups by size would save us from scanning the table when we know upfront no suitable groups exists.

## Running locally

- Install erlang 24.0.1 and elixir 1.12.0-otp-24

  if you are using `asdf` version manager simply run `asdf install`

- Install dependencies with `mix deps.get`
- Start the application with `MIX_ENV=prod mix phx.server`

Now requests can be made to [`localhost:9091`](http://localhost:9091)

## Performance

In the matter of performance, bellow there are some baseline tests on how the solution performs.
To keep it simple the tests are on the Domain and does not include the web and network times. Also these tests depends on the deployment.
I presume the real times are at least double in most of the cases.

The performance tests can be run locally by running:

`mix run test/performance.exs`

|    cars | journeys | load time | journeys creation | dropoff | locate |
| ------: | -------: | --------: | ----------------: | ------: | -----: |
|    1000 |     1000 |   1.278ms |           4.121ms |     5µs |    0µs |
|    1000 |    10000 |     948µs |           28.02ms |    19µs |    0µs |
|    1000 |   100000 |   2.141ms |         280.555ms |    31µs |    1µs |
|    1000 |  1000000 |  16.696ms |         3.050792s |    29µs |    1µs |
|   10000 |     1000 | 343.118ms |           4.175ms |     7µs |    1µs |
|   10000 |    10000 |   9.446ms |          48.642ms |     5µs |    0µs |
|   10000 |   100000 |  10.049ms |         316.299ms |    36µs |    1µs |
|   10000 |  1000000 |  26.951ms |         3.048388s |    28µs |    1µs |
|  100000 |     1000 | 435.943ms |           4.311ms |     7µs |    1µs |
|  100000 |    10000 | 120.573ms |          56.302ms |     8µs |    1µs |
|  100000 |   100000 |  118.04ms |          1.23335s |     8µs |    1µs |
|  100000 |  1000000 | 136.054ms |         4.405518s |    35µs |    1µs |
| 1000000 |     1000 | 1.938855s |           4.657ms |     9µs |    1µs |
| 1000000 |    10000 | 1.885688s |          69.413ms |     8µs |    1µs |
| 1000000 |   100000 | 1.896724s |         2.608952s |     9µs |    1µs |
| 1000000 |  1000000 | 1.944115s |        1.7876101m |    12µs |    1µs |
