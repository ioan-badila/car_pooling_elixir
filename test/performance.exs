defmodule CarPooling.Performance do
  @doc false

  def execute() do
    pre = """
    | cars | journeys| load_time | journeys creation| dropoff | locate |
    |-----:|--------:|----------:|-----------------:|--------:|-------:|
    """

    results =
      for cars <- [1_000, 10_000, 100_000, 1_000_000],
          journeys <- [1_000, 10_000, 100_000, 1_000_000],
          reduce: pre do
        acc -> acc <> run(cars, journeys)
      end

    IO.puts(results)
  end

  defp run(cars, journeys) do
    load_time = load_cars(cars)
    journeys_time = journeys(journeys)
    {load_time, journeys_time}
    drop_time = dropoff(1)
    locate_time = locate(2)

    ~s(|#{cars}|#{journeys}|#{load_time}|#{journeys_time}|#{drop_time}|#{locate_time}\n)
  end

  defp load_cars(count) do
    cars = for id <- 1..count//1, do: {id, Enum.random(4..6)}
    {time, :ok} = :timer.tc(CarPooling, :load_cars, [cars])
    format_time(time)
  end

  defp journeys(count) do
    time =
      Enum.reduce(1..count, 0, fn id, acc ->
        {time, :ok} = :timer.tc(CarPooling, :journey, [id, Enum.random(1..6)])
        time + acc
      end)

    format_time(time)
  end

  defp dropoff(group_id) do
    {time, :ok} = :timer.tc(CarPooling, :dropoff, [group_id])
    format_time(time)
  end

  defp locate(group_id) do
    {time, {:ok, _}} = :timer.tc(CarPooling, :locate, [group_id])
    format_time(time)
  end

  defp format_time(microseconds) when microseconds > 60_000_000,
    do: "#{microseconds / 60_000_000}m"

  defp format_time(microseconds) when microseconds > 1_000_000, do: "#{microseconds / 1_000_000}s"
  defp format_time(microseconds) when microseconds > 1000, do: "#{microseconds / 1_000}ms"
  defp format_time(microseconds), do: "#{microseconds}µs"
end

CarPooling.Performance.execute()
