defmodule CarPooling.Service.GroupsOfPeopleTest do
  @doc false

  use ExUnit.Case, async: false

  alias CarPooling.Service.GroupsOfPeople

  setup do
    GroupsOfPeople.reset()
  end

  describe "add_new/3" do
    test "adds a new group with default status :waiting" do
      assert GroupsOfPeople.add_new(1)
      assert [{1, :waiting}] = :ets.tab2list(GroupsOfPeople)
    end

    test "returns false when the group was already added" do
      assert GroupsOfPeople.add_new(1)
      refute GroupsOfPeople.add_new(1)
    end
  end

  test "set_status/3 updates a group with new status" do
    assert GroupsOfPeople.add_new(1)
    assert :ok = GroupsOfPeople.set_status(1, {:on_journey, 42, 4})
    assert [{1, {:on_journey, 42, 4}}] = :ets.tab2list(GroupsOfPeople)
  end

  test "get_status/2 returns the requested group status or nil" do
    assert GroupsOfPeople.add_new(1)
    assert :waiting = GroupsOfPeople.get_status(1)
    refute GroupsOfPeople.get_status(42)
  end

  test "remove_group/2" do
    assert GroupsOfPeople.add_new(1)
    assert :ok = GroupsOfPeople.remove(1)
    refute GroupsOfPeople.get_status(1)
    assert :ok = GroupsOfPeople.remove(1)
  end
end
