defmodule CarPooling.Service.CarsTest do
  @doc false

  use ExUnit.Case, async: false

  alias CarPooling.Service.Cars

  setup do
    Cars.reset()
  end

  test "add_new({car_id, seats})" do
    assert Cars.add_new({42, 5})
    assert :ets.tab2list(:cars) == [{42, 5, 5}]
    assert :ets.tab2list(:cars_bucket_5) == [{42}]
  end

  test "fetch/1" do
    assert Cars.add_new({42, 5})
    assert Cars.fetch(42) == {:ok, %{id: 42, seats: 5}}
    assert Cars.fetch(999) == {:error, :not_found}
  end

  test "update/2" do
    assert Cars.add_new({42, 5})
    assert Cars.update(42, 3)
    assert :ets.tab2list(:cars) == [{42, 5, 3}]
    assert :ets.tab2list(:cars_bucket_3) == [{42}]
  end

  test "reset/0" do
    assert Cars.add_new({42, 5})
    assert :ok = Cars.reset()
    assert :ets.tab2list(:cars) == []
    assert :ets.tab2list(:cars_bucket_1) == []
    assert :ets.lookup(:counters, :seats_count) == [{:seats_count, 0, 0, 0, 0, 0, 0, 0}]
  end

  test "find_car_for/2" do
    assert Cars.add_new({42, 5})
    assert Cars.find_car_for(4) == 42
    assert :ets.tab2list(:cars) == [{42, 5, 1}]
    assert :ets.tab2list(:cars_bucket_5) == []
    assert :ets.tab2list(:cars_bucket_1) == [{42}]
    assert :ets.lookup(:counters, :seats_count) == [{:seats_count, 0, 1, 0, 0, 0, 0, 0}]
  end

  test "restore_seats/2" do
    assert Cars.add_new({42, 5})
    assert Cars.find_car_for(4) == 42
    assert Cars.restore_seats(42, 4) == 5
  end
end
