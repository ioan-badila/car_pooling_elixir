defmodule CarPooling.Service.CarsBucketTest do
  @doc false

  use ExUnit.Case, async: false

  alias CarPooling.Service.CarsBucket

  setup do
    CarsBucket.reset()
  end

  test "insert/2" do
    assert CarsBucket.insert(1, 42)
    assert :ets.tab2list(:cars_bucket_1) == [{42}]
    assert :ets.lookup(:counters, :seats_count) == [{:seats_count, 0, 1, 0, 0, 0, 0, 0}]
  end

  test "reset/0" do
    assert CarsBucket.insert(1, 42)
    assert :ok = CarsBucket.reset()
    assert :ets.tab2list(:cars_bucket_1) == []
    assert :ets.lookup(:counters, :seats_count) == [{:seats_count, 0, 0, 0, 0, 0, 0, 0}]
  end
end
