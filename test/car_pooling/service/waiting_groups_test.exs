defmodule CarPooling.Service.WaitingGroupsTest do
  @doc false

  use ExUnit.Case, async: false

  alias CarPooling.Service.WaitingGroups

  setup do
    WaitingGroups.reset()
  end

  test "queue_group/1 adds a group to the queue" do
    assert :ok = WaitingGroups.queue_group(1, 5)
    assert [{_, 1, 5}] = :ets.tab2list(WaitingGroups)
  end

  test "queue_group/1 queues the groups in order of arrival" do
    assert :ok = WaitingGroups.queue_group(1, 5)
    assert :ok = WaitingGroups.queue_group(2, 4)
    assert :ok = WaitingGroups.queue_group(3, 6)
    assert [{_, 1, 5}, {_, 2, 4}, {_, 3, 6}] = :ets.tab2list(WaitingGroups)
  end

  test "reset/0 removes all groups from the queue" do
    assert :ok = WaitingGroups.queue_group(1, 5)
    assert :ok = WaitingGroups.queue_group(2, 4)
    assert :ok = WaitingGroups.reset()
    assert [] = :ets.tab2list(WaitingGroups)
  end

  test "find_and_delete_for/1 finds the oldest waiting group that fits the seats" do
    assert :ok = WaitingGroups.queue_group(1, 5)
    assert :ok = WaitingGroups.queue_group(2, 4)
    assert :ok = WaitingGroups.queue_group(3, 6)

    assert {1, 5} = WaitingGroups.find_and_delete_for(6)
    assert [{_, 2, 4}, {_, 3, 6}] = :ets.tab2list(WaitingGroups)
  end

  test "find_and_delete_for/1 returns nil if no group was found" do
    assert :ok = WaitingGroups.queue_group(1, 5)
    assert :ok = WaitingGroups.queue_group(2, 4)

    refute WaitingGroups.find_and_delete_for(3)
    assert [{_, 1, 5}, {_, 2, 4}] = :ets.tab2list(WaitingGroups)
  end
end
