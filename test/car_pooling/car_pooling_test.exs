defmodule CarPoolingTest do
  @doc false

  use ExUnit.Case, async: false

  describe "load_cars/1" do
    test "adds the cars and resets the tables" do
      CarPooling.load_cars(%{1 => 4})
      assert CarPooling.journey(1, 3) == :ok
      assert CarPooling.locate(1) == {:ok, %{id: 1, seats: 4}}
      CarPooling.load_cars(%{2 => 5})
      assert CarPooling.locate(1) == {:error, :not_found}
    end
  end

  describe "journey/1" do
    test "schedule a journey for a group" do
      CarPooling.load_cars(%{1 => 4})
      assert CarPooling.journey(1, 5) == :ok
    end

    test "returns an error if the group is already on a journey" do
      CarPooling.load_cars(%{1 => 4})
      assert CarPooling.journey(1, 5) == :ok
      assert {:error, :is_invalid, _message} = CarPooling.journey(1, 5)
    end
  end

  describe "dropoff/1" do
    test "a group on a journey" do
      CarPooling.load_cars(%{1 => 4})
      CarPooling.journey(1, 4)
      assert CarPooling.locate(1) == {:ok, %{id: 1, seats: 4}}
      assert CarPooling.dropoff(1) == :ok
      assert CarPooling.locate(1) == {:error, :not_found}
    end

    test "a waiting group has no effect" do
      CarPooling.load_cars(%{1 => 4})
      CarPooling.journey(1, 5)
      assert CarPooling.locate(1) == {:ok, :waiting}
      assert CarPooling.dropoff(1) == :ok
      assert CarPooling.locate(1) == {:ok, :waiting}
    end

    test "a non-existing group returns an error" do
      CarPooling.load_cars(%{1 => 4})
      assert CarPooling.dropoff(1) == {:error, :not_found}
    end
  end

  describe "locate/1" do
    test "returns the car the group is riding" do
      CarPooling.load_cars(%{1 => 4})
      CarPooling.journey(1, 4)
      assert CarPooling.locate(1) == {:ok, %{id: 1, seats: 4}}
    end

    test "returns waiting for groups no yet on a journey" do
      CarPooling.load_cars(%{1 => 4})
      CarPooling.journey(1, 5)
      assert CarPooling.locate(1) == {:ok, :waiting}
    end

    test "returns an error for a non-existing group" do
      CarPooling.load_cars(%{1 => 4})
      assert CarPooling.dropoff(1) == {:error, :not_found}
    end
  end

  describe "Scenarios" do
    test "Uses the smallest suitable car" do
      cars = %{1 => 5, 2 => 5, 3 => 6, 4 => 6, 5 => 4}
      CarPooling.load_cars(cars)
      CarPooling.journey(1, 3)
      assert CarPooling.locate(1) == {:ok, %{id: 5, seats: 4}}
    end

    test "Uses all the seats in a car if possible" do
      CarPooling.load_cars(%{6 => 5})
      CarPooling.journey(1, 2)
      CarPooling.journey(2, 1)
      CarPooling.journey(3, 2)
      assert CarPooling.locate(1) == {:ok, %{id: 6, seats: 5}}
      assert CarPooling.locate(2) == {:ok, %{id: 6, seats: 5}}
      assert CarPooling.locate(3) == {:ok, %{id: 6, seats: 5}}
    end

    test "Uses the cars with less seats available" do
      cars = %{1 => 4, 2 => 4, 3 => 4}
      CarPooling.load_cars(cars)
      CarPooling.journey(1, 3)
      CarPooling.journey(2, 1)
      assert {:ok, %{id: 2, seats: car_id}} = CarPooling.locate(1)
      assert {:ok, %{id: 2, seats: ^car_id}} = CarPooling.locate(2)
    end

    test "Uses all the available cars seats" do
      cars = %{1 => 4, 2 => 5, 3 => 6}
      CarPooling.load_cars(cars)
      CarPooling.journey(1, 3)
      CarPooling.journey(2, 3)
      CarPooling.journey(3, 1)
      CarPooling.journey(4, 1)
      CarPooling.journey(5, 2)
      CarPooling.journey(6, 1)
      CarPooling.journey(7, 4)
      assert CarPooling.locate(1) == {:ok, %{id: 1, seats: 4}}
      assert CarPooling.locate(2) == {:ok, %{id: 2, seats: 5}}
      assert CarPooling.locate(3) == {:ok, %{id: 1, seats: 4}}
      assert CarPooling.locate(4) == {:ok, %{id: 2, seats: 5}}
      assert CarPooling.locate(5) == {:ok, %{id: 3, seats: 6}}
      assert CarPooling.locate(6) == {:ok, %{id: 2, seats: 5}}
      assert CarPooling.locate(7) == {:ok, %{id: 3, seats: 6}}
    end

    test "The waiting groups are assigned based on priority and seats availability" do
      CarPooling.load_cars(%{4 => 6})
      CarPooling.journey(1, 6)
      assert CarPooling.locate(1) == {:ok, %{id: 4, seats: 6}}
      CarPooling.journey(2, 3)
      assert CarPooling.locate(2) == {:ok, :waiting}
      CarPooling.journey(3, 6)
      assert CarPooling.locate(3) == {:ok, :waiting}
      CarPooling.journey(4, 3)
      assert CarPooling.locate(4) == {:ok, :waiting}
      CarPooling.dropoff(1)
      assert CarPooling.locate(2) == {:ok, %{id: 4, seats: 6}}
      assert CarPooling.locate(3) == {:ok, :waiting}
      assert CarPooling.locate(4) == {:ok, %{id: 4, seats: 6}}
    end

    test "After dropoff try to reassign the cars with less seats available" do
      cars = %{1 => 4, 2 => 6}
      CarPooling.load_cars(cars)
      CarPooling.journey(1, 5)
      CarPooling.journey(2, 1)
      CarPooling.dropoff(1)
      CarPooling.journey(3, 4)
      CarPooling.journey(4, 5)
      assert CarPooling.locate(2) == {:ok, %{id: 2, seats: 6}}
      assert CarPooling.locate(3) == {:ok, %{id: 1, seats: 4}}
      assert CarPooling.locate(4) == {:ok, %{id: 2, seats: 6}}
    end

    test "After dropoff free car seats are restored" do
      CarPooling.load_cars(%{1 => 5})
      CarPooling.journey(1, 3)
      CarPooling.journey(2, 2)
      CarPooling.dropoff(1)
      CarPooling.journey(3, 4)
      CarPooling.journey(4, 3)
      assert CarPooling.locate(3) == {:ok, :waiting}
      assert CarPooling.locate(4) == {:ok, %{id: 1, seats: 5}}
    end
  end
end
