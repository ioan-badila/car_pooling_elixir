defmodule CarPoolingWeb.Controllers.ServiceControllerTest do
  @moduledoc false

  use CarPoolingWeb.ConnCase, async: false

  describe "load_cars" do
    @valid [
      %{
        "id" => 1,
        "seats" => 4
      },
      %{
        "id" => 2,
        "seats" => 6
      }
    ]

    test "correctly loads the cars", %{conn: conn} do
      assert conn
             |> load_cars(@valid)
             |> response(200)
    end

    test "returns an error when input is not a list of cars", %{conn: conn} do
      load_cars_expect_400(conn, nil)
      load_cars_expect_400(conn, "boom")
    end

    test "returns an error when at least one car is invalid", %{conn: conn} do
      load_cars_expect_400(conn, ["boom"])
      load_cars_expect_400(conn, [nil])
      load_cars_expect_400(conn, [%{}])
      load_cars_expect_400(conn, [%{"id" => "r", "seats" => 4}])
      load_cars_expect_400(conn, [%{"id" => 0, "seats" => 4}])
      load_cars_expect_400(conn, [%{"id" => 1, "seats" => "r"}])
      load_cars_expect_400(conn, [%{"id" => 1, "seats" => 3}])
    end

    defp load_cars_expect_400(conn, cars) do
      conn
      |> load_cars(cars)
      |> response(400)
    end

    defp load_cars(conn, cars) do
      put(conn, Routes.service_path(conn, :load_cars), _json: cars)
    end
  end

  describe "journey" do
    setup %{conn: conn} do
      put(conn, Routes.service_path(conn, :load_cars), _json: [%{"id" => 1, "seats" => 4}])
      %{conn: conn}
    end

    test "correctly schedule a journey", %{conn: conn} do
      conn
      |> journey(%{"id" => 1, "people" => 5})
      |> response(200)
    end

    test "returns an error when params are invalid", %{conn: conn} do
      journey_expect_400(conn, nil)
      journey_expect_400(conn, %{"id" => "r", "people" => 2})
      journey_expect_400(conn, %{"id" => 0, "people" => 3})
      journey_expect_400(conn, %{"id" => 1, "people" => "r"})
      journey_expect_400(conn, %{"id" => 1, "people" => 9})
      journey_expect_400(conn, %{"id" => 1, "people" => 0})
    end

    defp journey_expect_400(conn, params) do
      conn
      |> journey(params)
      |> response(400)
    end

    defp journey(conn, params) do
      post(conn, Routes.service_path(conn, :journey), params)
    end
  end

  describe "dropoff" do
    setup %{conn: conn} do
      put(conn, Routes.service_path(conn, :load_cars), _json: [%{"id" => 1, "seats" => 4}])
      post(conn, Routes.service_path(conn, :journey), %{"id" => 1, "people" => 3})
      post(conn, Routes.service_path(conn, :journey), %{"id" => 2, "people" => 5})

      conn = conn |> put_req_header("content-type", "application/x-www-form-urlencoded")

      {:ok, conn: conn}
    end

    test "drops off a group of people that were on a journey", %{conn: conn} do
      dropoff(conn, "ID=1") |> response(200)
    end

    test "ignores group of people that were waiting for a car", %{conn: conn} do
      dropoff(conn, "ID=2") |> response(200)
    end

    test "returns an error when the group is not to be found", %{conn: conn} do
      dropoff(conn, "ID=42") |> response(404)
    end

    test "returns an error when when params are invalid", %{conn: conn} do
      dropoff_expect_400(conn, nil)
      dropoff_expect_400(conn, "")
      dropoff_expect_400(conn, "ID=0")
      dropoff_expect_400(conn, "ID=s")
      dropoff_expect_400(conn, "id=1")
    end

    defp dropoff_expect_400(conn, params) do
      conn
      |> dropoff(params)
      |> response(400)
    end

    defp dropoff(conn, params) do
      post(conn, Routes.service_path(conn, :dropoff), params)
    end
  end

  describe "locate" do
    @car %{"id" => 1, "seats" => 4}
    setup %{conn: conn} do
      put(conn, Routes.service_path(conn, :load_cars), _json: [@car])
      post(conn, Routes.service_path(conn, :journey), %{"id" => 1, "people" => 3})
      post(conn, Routes.service_path(conn, :journey), %{"id" => 2, "people" => 5})

      conn = conn |> put_req_header("content-type", "application/x-www-form-urlencoded")

      {:ok, conn: conn}
    end

    test "returns the car as the payload when the group is assigned to a car", %{conn: conn} do
      assert locate(conn, "ID=1") |> json_response(200) == @car
    end

    test "returns 204 when the group is waiting to be assigned to a car", %{conn: conn} do
      locate(conn, "ID=2") |> response(204)
    end

    test "returns an error when the group is not to be found", %{conn: conn} do
      locate(conn, "ID=42") |> response(404)
    end

    test "returns an error when when params are invalid", %{conn: conn} do
      locate_expect_400(conn, nil)
      locate_expect_400(conn, "")
      locate_expect_400(conn, "ID=0")
      locate_expect_400(conn, "ID=s")
      locate_expect_400(conn, "id=1")
    end

    defp locate_expect_400(conn, params) do
      conn
      |> locate(params)
      |> response(400)
    end

    defp locate(conn, params) do
      post(conn, Routes.service_path(conn, :locate), params)
    end
  end
end
