use Mix.Config

config :car_pooling, CarPoolingWeb.Endpoint,
  http: [port: 9091],
  server: false

config :logger, level: :warn
