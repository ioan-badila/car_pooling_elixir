use Mix.Config

config :car_pooling, CarPoolingWeb.Endpoint,
  http: [port: 9091],
  debug_errors: true,
  code_reloader: true,
  check_origin: false,
  watchers: []

config :logger, :console, format: "[$level] $message\n"

config :phoenix, :stacktrace_depth, 20

config :phoenix, :plug_init_mode, :runtime
